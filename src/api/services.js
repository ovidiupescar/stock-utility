const Pool = require('pg').Pool;

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
  })

// -- get products--

const getProducts = async () => {
    const products = await pool.query('select * from products');
    return {
      data: products.rows
    }
}

// --get one especific product
const getProduct = async (productId) => {
    const result = await pool.query('select * from products where id=$1', [productId])
    return {
      data: result.rows[0]
    }
}

// --delete product

const deleteProduct = async (productId) => {
    const product = await pool.query('delete from products where id=$1', [productId])
}

// -- create product

const createProduct = async ({name, description, units}) => { //se desestructura el objeto body
    const product = await pool.query('insert into products (name, description, units)   values($1, $2, $3)',
    [name, description, units])
}

// --update product

const updateProduct = async (productId, {name, description, units}) => {
    const product = await pool.query('update products set name=($2), description=($3), units=($4) where id=$1',
    [productId, name, description, units])
}

module.exports = {
    getProducts,
    getProduct,
    deleteProduct,
    createProduct,
    updateProduct
}