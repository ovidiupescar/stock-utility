export class InMemoryDataService {
  createDb() {
    const products = [
      { id: 0, name: 'Patata', description: 'Blanca', units: 10 },
      { id: 1, name: 'Cebolla', description: 'Roja', units: 50 },
      { id: 2, name: 'Ajo', description: 'Negro', units: 25 },
      { id: 3, name: 'Sardinas', description: 'Frescas', units: 40 },
      { id: 4, name: 'Bollas', description: 'Bollas de pan', units: 5 },
      { id: 5, name: 'Baguette', description: '', units: 10 },
      { id: 6, name: 'Lata de tomate', description: 'Lata de tomate de 1kg', units: 15 },
    ];
    return {products};
  }
}