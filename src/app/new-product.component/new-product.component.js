"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var product_service_1 = require("./product.service");
var NewProductComponent = (function () {
    function NewProductComponent(productService, router) {
        this.productService = productService;
        this.router = router;
    }
    NewProductComponent.prototype.getProduct = function () {
        var _this = this;
        this.productService
            .getProducts()
            .subscribe(function (products) { return _this.products = products; });
    };
    NewProductComponent.prototype.gotoProducts = function () {
        this.router.navigate(['/products']);
    };
    NewProductComponent.prototype.add = function (name, description, units) {
        var _this = this;
        name = name.trim();
        description = description.trim();
        if (!name || !units) {
            return;
        }
        ;
        this.productService.create(name, description, units)
            .subscribe(function (product) {
            _this.products = []; //need to start an empty array 
            _this.products.push(product);
        });
        this.gotoProducts();
    };
    return NewProductComponent;
}());
NewProductComponent = __decorate([
    core_1.Component({
        selector: 'newProduct',
        templateUrl: './newProduct.component.html',
        styleUrls: ['./newProduct.component.css']
    }),
    __metadata("design:paramtypes", [product_service_1.ProductService,
        router_1.Router])
], NewProductComponent);
exports.NewProductComponent = NewProductComponent;
//# sourceMappingURL=newProduct.component.js.map