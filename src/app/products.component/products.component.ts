import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

 
import { Product }                from '../product/product';
import { ProductService }         from '../product.service/product.service';
 
@Component({
  selector: 'my-products',
  templateUrl: './products.component.html',
  styleUrls: [ './products.component.css' ]
})
export class ProductsComponent implements OnInit {
  products: Product[];
  selectedProduct: Product;
 
  constructor(
    private productService: ProductService,
    private router: Router) { }
 
  getProduct(): void {
    this.productService
        .getProducts()
        .subscribe(products => {
          return this.products = products
        });
  }
 
  add(name: string, description: string, units: number): void {
    name = name.trim();
    description = description.trim();
    if (!name || !units) { return; }
    this.productService.createProduct(name, description, units)
      .subscribe(product => {
        this.products.push(product);
        this.selectedProduct = null;
      });
  }
 
  delete(product: Product): void {
    this.productService
        .deleteProduct(product.id)
        .then(() => {
          this.products = this.products.filter(h => h !== product);
          this.selectedProduct = null;
        });
  }
 
  ngOnInit(): void {
    this.getProduct();
  }
 
  onSelect(product: Product): void {
    this.selectedProduct = product;
  }
 
  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedProduct.id]);
  }

  info(product: Product): void { //to go to details directly
    this.onSelect(product);
    this.gotoDetail();
  }
  deleteProduct(product: Product): void { //to delete the selected product
    this.delete(this.selectedProduct);
  }
}
