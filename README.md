# Stock

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3, to deploy on heroku. You can go to https://stock-utility.herokuapp.com/ to check the result.

## Pre-requisites

To run in local, you need to install `Node.js` and `npm`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.


